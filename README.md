# SSO in one go
A dandy little document to stop you copy/pasting SSO credentials every time the token expires. Just type `sso` and away it goes!

p.s. written for linux, untested on windows.

p.p.s. if you have a better way, submit a PR! 

## Setup AWS Credential file
Add the sso variables to your credentials, and remove the `aws_access_key`, `aws_secret_access_key`, and `aws_session_token`.
Add the `sso_start_url`, `sso_region`, `sso_account_id`, `sso_role_name` like below:

**Note**: sso_role_name is the name of the role you choose on awsapps, such as PowerUser/ReadOnly/Developer

**Note**: {SUBDOMAIN} needs to be replaced with relevant subdomain.

```shell
[RND_DevOps]
sso_start_url = https://{SUBDOMAIN}.awsapps.com/start
sso_region = eu-west-1
sso_account_id = 012345678910
sso_role_name = DevOps

[SANDBOX_DevOps]
sso_start_url = https://{SUBDOMAIN}.awsapps.com/start
sso_region = eu-west-1
sso_account_id = 019876543210
sso_role_name = DevOps
```

At this point, you can run

```shell
$ aws sso login --profile RND_DevOps
```

## Make a helper script
Run these commands to create a new executable and add to your system
```shell
$ echo '#!/bin/bash

for cmd in "$@"; do {
  echo "Process \"$cmd\" started";
  $cmd & pid=$!
  PID_LIST+=" $pid";
} done

trap "kill $PID_LIST" SIGINT

echo "Parallel processes have started";

wait $PID_LIST

echo
echo "All processes have completed";' > parrallel_commands
```
```shell
$ chmod +x parrallel_commands
```
```shell
$ sudo mv parrallel_commands /usr/local/bin
```

Add the below code to your `~/.bashrc`, edit the `PARRALLEL_AWS_SSO_PROFILES` as needed:

```shell
PARRALLEL_AWS_SSO_PROFILES=("RND_DevOps" "SANDBOX_DevOps" )
PARRALLEL_AWS_SSO="aws sso login --profile"
PARRALLEL_CMD=""
for i in "${PARRALLEL_AWS_SSO_PROFILES[@]}"
do 
    PARRALLEL_CMD=$PARRALLEL_CMD" '$PARRALLEL_AWS_SSO $i' "
done
alias sso="parrallel_commands $PARRALLEL_CMD"
```

Run the below command to make `sso` avaliable to your command line:
```shell
$ source ~/.bashrc
```

Login to SSO easily: 

```shell
$ sso
```